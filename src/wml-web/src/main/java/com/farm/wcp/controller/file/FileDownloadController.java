package com.farm.wcp.controller.file;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.farm.authority.FarmAuthorityService;
import com.farm.authority.domain.User;
import com.farm.file.FarmFileServiceInter;
import com.farm.file.domain.FileBase;
import com.farm.file.domain.ex.PersistFile;
import com.farm.file.exception.FileExNameException;
import com.farm.file.service.VisitServiceInter;
import com.farm.file.service.VisitServiceInter.OpModel;
import com.farm.file.util.FarmDocFiles;
import com.farm.parameter.FarmParameterService;
import com.farm.wcp.util.DownloadUtils;
import com.farm.wcp.util.HttpContentType;
import com.farm.wcp.util.ThumbnailsUtils;
import com.farm.web.WebUtils;


@RequestMapping("/download")
@Controller
public class FileDownloadController extends WebUtils {
	private static final Logger log = Logger.getLogger(FileDownloadController.class);

	@Resource
	private FarmFileServiceInter farmFileServiceImpl;
	@Resource
	private VisitServiceInter visitServiceImpl;

	
	@RequestMapping("/Pubfile")
	public void loadFileByDown(String id, String secret, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) throws Exception {
		PersistFile file = farmFileServiceImpl.getPersistFile(id);
//		if (!file.getSecret().trim().equals(secret)) {
//			response.sendError(405, "安全码错误!secret：" + secret);
//			return;
//		}
		visitServiceImpl.record(id, OpModel.DOWN, getCurrentUser(session));
		DownloadUtils.downloadFile(file.getFile(), file.getName(), response);
	}

	
	@RequestMapping("/Pubload")
	public void loadFileByFile(String id, String secret, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) throws FileNotFoundException, IOException {
		PersistFile file = farmFileServiceImpl.getPersistFile(id);
		// if (!file.getSecret().trim().equals(secret)) {
		// response.sendError(405, "安全码错误!secret：" + secret);
		// return;
		// }
		visitServiceImpl.record(id, OpModel.VIEW, getCurrentUser(session));
		DownloadUtils.sendVideoFile(request, response, file.getFile(), file.getName(),
				new HttpContentType().getContentType(farmFileServiceImpl.getExName(file.getName())));
	}

	// ---------------------------------------------------------------
	
	@RequestMapping("/PubPhoto")
	public void loadPhotoByDown(String id, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) throws Exception {
		if (StringUtils.isNotBlank(id)) {
			// 支持通过登陆名下载头像
			User user = (User) FarmAuthorityService.getInstance().getUserById(id);
			if (user != null && StringUtils.isNotBlank(user.getImgid())) {
				id = user.getImgid();
			}
		}
		File backFile = null;
		String backFileName = null;
		FileBase fileb = farmFileServiceImpl.getFileBase(id);
		if (fileb != null) {
			loadimg(id, "64", request, response, session);
		}
		String imgpath = FarmParameterService.getInstance().getParameter("config.doc.none.photo.path");
		File defaultPhone = new File(FarmParameterService.getInstance().getParameter("farm.constant.webroot.path")
				+ File.separator + imgpath.replaceAll("\\\\", File.separator).replaceAll("//", File.separator));
		backFile = defaultPhone;
		backFileName = "默认头像.png";
		DownloadUtils.downloadFile(backFile, backFileName, response);
	}

	
	@RequestMapping("/Pubimg")
	public void loadimg(String fileid, String width, HttpServletRequest request, HttpServletResponse response,
			HttpSession session) throws Exception {
		PersistFile file = farmFileServiceImpl.getPersistFile(fileid);
		File backFile = file.getFile();
		String backFileName = file.getName();
		if (file == null || !backFile.exists()) {
			throw new RuntimeException("the file is not exist!");
		}
		if (width != null) {
			try {
				backFile = ThumbnailsUtils.trimImg(FarmDocFiles.getExName(backFileName), width, backFile, null);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		} else {
			if (getCurrentUser(session) != null) {
				visitServiceImpl.record(fileid, OpModel.VIEW, getCurrentUser(session));
			}
		}
		DownloadUtils.downloadFile(backFile, backFileName, response);
	}

}
