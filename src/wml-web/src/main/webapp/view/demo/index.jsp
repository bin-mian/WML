<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<PF:basePath/>">
<title>事件模型数据管理</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<script src="text/lib/echarts/echarts.min.4.4.0.js"></script>
<link rel="icon" href="favicon.ico" mce_href="favicon.ico"
	type="image/x-icon">
<link rel="shortcut icon" href="favicon.ico" mce_href="favicon.ico"
	type="image/x-icon">


<script type="text/javascript" src="text/javascript/jquery1113.js"></script>
<link href="text/lib/bootstrap3/css/bootstrap.min.css" rel="stylesheet">
<link href="text/lib/bootstrap3/css/bootstrap-theme.min.css"
	rel="stylesheet">
<script src="text/lib/bootstrap3/js/bootstrap.min.js"></script>
<link href="view/web-simple/atext/style/web-base.css" rel="stylesheet">
<script type="text/javascript">
	var basePath = '<PF:basePath/>';
	$(function() {
		$.ajaxSetup({
			cache : false
		});
	})
//-->
</script>
</head>
<body>
	<div class="list-group" style="margin: 20px; padding: 20px;">
		<a href="demo/echartsLine.do" class="list-group-item active">echarts-line</a>
		<a href="demo/wordcloud.do" class="list-group-item">wordCloud</a> <a
			href="demo/localInit.do" class="list-group-item">本系统初始化</a>
	</div>
</body>
</html>