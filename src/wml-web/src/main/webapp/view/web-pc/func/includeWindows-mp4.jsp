<%@page import="com.farm.parameter.FarmParameterService"%>
<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<script src="text/lib/dplayer/DPlayer.min.js"></script>
<div style="background: #000000;">
	<div id="player1" class="dplayer" ></div>
</div>
<script type="text/javascript">
	function loadPlayer(fileurl) {
		var height = $(window).height()/2;
		
		$('#player1').css('height', height);
		var dp = new DPlayer({
			element : document.getElementById('player1'),
			autoplay : false,
			theme : '#FADFA3',
			loop : false,
			lang : 'zh-cn',
			screenshot : false,
			hotkey : true,
			preload : 'auto',
			video : {
				url : fileurl,
				pic : 'text/img/demo/playerIcon.png'
			},
			contextmenu : []
		});
		dp.on("ended", function() {
			openOperatorWin();
		});

	}
	//是否播放状态
	function isPlay() {
		return true;
	}
	//停止视频播放
	function stopPlay() {

	}
	//获得当前播放时间（秒）
	function getPlayedSeccend() {
		return 0;
	}
	//获得视频总长度（秒）
	function getPlayDuration() {
		return 100;
	}
	//跳转到某个时间点
	function seekPlayedSeccend(seccend) {
		return true;
	}
</script>
