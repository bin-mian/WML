<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!-- 内容顶端导航的扩展窗口，可以添加各种筛选控件 -->
<!-- 上传文件窗口 -->
<div class="wml_ui_plusWin" id="wml_ui_plusWin_upload">
	<jsp:include page="/view/web-pc/func/includeUploadBox.jsp"></jsp:include>
</div>
<!-- 列选择窗口 -->
<div class="wml_ui_plusWin" id="wml_ui_plusWin_column">
	<div style="padding: 34px; padding-left: 25px;">
		<div class="form-check form-check-inline">
			<input class="form-check-input" checked="checked" type="radio"
				name="wml_input_colum" id="inlineColumnRadio1" value="none">
			<label class="form-check-label" for="inlineColumnRadio1">默认</label>
		</div>
		<div class="form-check form-check-inline">
			<input class="form-check-input" type="radio" name="wml_input_colum"
				id="inlineColumnRadio2" value="1"> <label
				class="form-check-label" for="inlineColumnRadio2">1列</label>
		</div>
		<div class="form-check form-check-inline">
			<input class="form-check-input" type="radio" name="wml_input_colum"
				id="inlineColumnRadio3" value="2"> <label
				class="form-check-label" for="inlineColumnRadio3">2列</label>
		</div>
		<div class="form-check form-check-inline">
			<input class="form-check-input" type="radio" name="wml_input_colum"
				id="inlineColumnRadio4" value="4"> <label
				class="form-check-label" for="inlineColumnRadio4">4列</label>
		</div>
		<div class="form-check form-check-inline">
			<input class="form-check-input" type="radio" name="wml_input_colum"
				id="inlineColumnRadio5" value="6"> <label
				class="form-check-label" for="inlineColumnRadio5">6列</label>
		</div>
	</div>
</div>
<!-- 布局选择窗口 -->
<div class="wml_ui_plusWin" id="wml_ui_plusWin_layout">
	<div style="padding: 34px; padding-left: 25px;">
		<div class="form-check form-check-inline">
			<input class="form-check-input" checked="checked" type="radio"
				name="wml_input_layout" id="inlineLayoutRadio1" value="none">
			<label class="form-check-label" for="inlineLayoutRadio1">默认</label>
		</div>
		<div class="form-check form-check-inline">
			<input class="form-check-input" type="radio" name="wml_input_layout"
				id="inlineLayoutRadio2" value="grid"> <label
				class="form-check-label" for="inlineLayoutRadio2">网格模式</label>
		</div>
		<div class="form-check form-check-inline">
			<input class="form-check-input" type="radio" name="wml_input_layout"
				id="inlineLayoutRadio3" value="list"> <label
				class="form-check-label" for="inlineLayoutRadio3">列表模式</label>
		</div>
	</div>
</div>
<!-- 排序选择窗口 -->
<div class="wml_ui_plusWin" id="wml_ui_plusWin_sort">
	<div style="padding: 34px; padding-left: 25px;">
		<div class="form-check form-check-inline">
			<input class="form-check-input" checked="checked" type="radio"
				name="wml_input_sort" id="inlinesortRadio1" value="none"> <label
				class="form-check-label" for="inlinesortRadio1">默认</label>
		</div>
		<div class="form-check form-check-inline">
			<input class="form-check-input" type="radio" name="wml_input_sort"
				id="inlinesortRadio2" value="time"> <label
				class="form-check-label" for="inlinesortRadio2">创建时间</label>
		</div>
		<div class="form-check form-check-inline">
			<input class="form-check-input" type="radio" name="wml_input_sort"
				id="inlinesortRadio3" value="title"> <label
				class="form-check-label" for="inlinesortRadio3">文件名称</label>
		</div>
		<div class="form-check form-check-inline">
			<input class="form-check-input" type="radio" name="wml_input_sort"
				id="inlinesortRadio4" value="size"> <label
				class="form-check-label" for="inlinesortRadio4">文件大小</label>
		</div>
		<div class="form-check form-check-inline">
			<input class="form-check-input" type="radio" name="wml_input_sort"
				id="inlinesortRadio5" value="ex"> <label
				class="form-check-label" for="inlinesortRadio5">文件类型</label>
		</div>
	</div>
</div>
<!-- 多文件选择窗口 -->
<div class="wml_ui_plusWin" id="wml_ui_plusWin_select">
	<div
		style="text-align: center; border: 2px dashed #545558; height: 80px; margin: 10px; padding-top: 15px;">
		<div class="media" style="width: 380px; margin: auto;">
			<i style="font-size: 32px;" class="bi bi-files mr-3"></i>
			<div class="media-body" style="padding-top: 10px; text-align: left;">
				&nbsp;当前模式下可以多选文件操作 &nbsp;
				<button style="margin-top: -6px;" type="button"
					onclick="wmlDeleteMore()" class="btn btn-secondary">删除</button>
				&nbsp;
				<button style="margin-top: -6px;" type="button"
					onclick="wmlMoveMore()" class="btn btn-secondary">移动</button>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		function wmlDeleteMore() {
			var selected = getMoreSelectFileids();
			if (selected.num > 0) {
				wmlConfirm('确认删除选中的' + selected.num + '个文件吗?',
						'opfile/del.do?categrayFileId=' + selected.ids, 'ajax',
						'freshFiles');
			}
		}

		function wmlMoveMore() {
			var selected = getMoreSelectFileids();
			if (selected.num > 0) {
				wmlChosseCategray(selected.ids);
			}
		}

		function getMoreSelectFileids() {
			var ids;
			var idsNum = 0;
			$('.mactive', '.wml-ui-filsbox').each(function(i, obj) {
				idsNum++;
				var appid = $(obj).attr('data-fileid');
				if (ids) {
					ids = ids + "," + appid;
				} else {
					ids = appid;
				}
			});
			return {
				'ids' : ids,
				'num' : idsNum
			}
		}
	</script>
</div>