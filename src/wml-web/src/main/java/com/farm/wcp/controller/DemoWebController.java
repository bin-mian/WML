package com.farm.wcp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.farm.core.page.ViewMode;
import com.farm.web.WebUtils;

@RequestMapping("/demo")
@Controller
public class DemoWebController extends WebUtils {

	@RequestMapping("/PubHome")
	public ModelAndView PubHome(HttpServletRequest request, HttpSession session) {
		return ViewMode.getInstance().returnModelAndView("demo/index");
	}

	@RequestMapping("/echartsLine")
	public ModelAndView echartsLine(HttpServletRequest request, HttpSession session) {
		return ViewMode.getInstance().returnModelAndView("demo/echartsLine");
	}

	@RequestMapping("/wordcloud")
	public ModelAndView wordcloud(HttpServletRequest request, HttpSession session) {
		return ViewMode.getInstance().returnModelAndView("demo/wordcloud");
	}

	/**
	 * 本系统初始化
	 * 
	 * @param request
	 * @param session
	 * @return
	 */
	@RequestMapping("/localInit")
	public ModelAndView localInit(HttpServletRequest request, HttpSession session) {
		try {

			if (getCurrentUser(session) == null || !getCurrentUser(session).getType().equals("3")) {
				throw new RuntimeException("当前非管理员账户");
			}

			return ViewMode.getInstance().returnModelAndView("demo/localinit");
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnModelAndView("web-simple/simple-500");
		}

	}

}
