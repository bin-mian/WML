<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<style>
#evaluationObjStarId i {
	margin-left: 8px;
}

#evaluationObjStarId i:hover {
	font-weight: 700;
	color: #ffffff;
}
</style>
<hr style="margin-top: 8px; margin-bottom: 4px;" />
<h4
	title="<c:if test="${!empty praise}">我的评价${praise.intflag }</c:if><c:if test="${!empty visit&&!empty visit.praise}">/平均得分${visit.praise }</c:if>">
	评价&nbsp;${visit.praise }&nbsp;|&nbsp;评论&nbsp;
	<c:if test="${fn:length(comments)>0}"> ${fn:length(comments)}</c:if>
</h4>
<div>
	<span
		style="font-size: 24px; cursor: pointer; color: #ffc107; font-size: 12px;"
		id="evaluationObjStarId" data-max="5"> <!--                                           -->
		<c:if test="${!empty praise}">
			<c:forEach begin="1" end="7" varStatus="node">

				<i
					class="bi ${node.index<=praise.intflag?'bi-star-fill':'bi-star' }"
					data-val="${node.index}" onclick="wmlPraise('${node.index}')"></i>
			</c:forEach>
		</c:if> <!--                                      --> <c:if
			test="${empty praise}">
			<c:forEach begin="1" end="7" varStatus="node">
				<i class="bi ${node.index<=visit.praise?'bi-star-fill':'bi-star' }"
					data-val="${node.index}" onclick="wmlPraise('${node.index}')"></i>
			</c:forEach>
		</c:if> <!--                                      -->

	</span>
</div>
<script type="text/javascript">
	function wmlPraise(val) {
		$.post('opfile/praise.do', {
			'val' : val,
			'appId' : '${empty file.file.id?categray.id:file.file.id}'
		}, function(flag) {
			if (flag.STATE == 0) {
				$('i', '#evaluationObjStarId').each(function(i, obj) {
					if ($(obj).attr('data-val') <= val) {
						$(obj).removeClass('bi-star');
						$(obj).addClass('bi-star-fill');
					} else {
						$(obj).addClass('bi-star');
						$(obj).removeClass('bi-star-fill');
					}
				});
			} else {
				alert(flag.MESSAGE);
			}
		}, 'json')
	}
</script>