<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<form class="form-signin" role="form" id="loginFormId"
	action="login/websubmit.do" method="post">
	<input type="hidden" name="sysid" value="${sysid}"> <input
		type="hidden" name="outuserid" value="${outuserid}">
	<div class="form-group">
		<label for="exampleInputEmail1"> 登录名 </label> <input type="text"
			class="form-control" placeholder="手机/邮箱/用户登录名" value="${loginname}"
			style="margin-top: 4px;" id="loginNameId" name="name" required
			autofocus>
	</div>
	<div class="form-group">
		<label for="exampleInputEmail1"> 登录密码 </label> <input type="password"
			class="form-control" placeholder="请录入密码" style="margin-top: 4px;"
			id="loginPassWId" required> <input name="password"
			type="hidden" id="realPasswordId"><input name="sck" type="hidden"   value="${sck}">
	</div>
	<div class="form-group" id="loginCheckFormId" style="display: none;">
		<label for="exampleInputEmail1">验证码</label>
		<div class="input-group">
			<input type="text" class="form-control" placeholder="请录入验证码"
				id="checkcodeId" name="checkcode">
			<div class="input-group-addon" style="padding: 0px;">
				<img id="checkcodeimgId"
					style="cursor: pointer; height: 30px; width: 100px;"
					src="webfile/Pubcheckcode.do" />
			</div>
		</div>
	</div>
	<!-- <input type="hidden" name="url" id="loginUrlId"> -->
	<div>
		<button class="btn btn-danger text-left" id="loginButtonId"
			style="margin-top: 4px; width: 100%;" type="button">登录</button>
	</div>
	<div style="text-align: right; padding: 4px;">
		<PF:IfParameterEquals key="config.show.local.regist.able" val="true">
			&nbsp;<a style="margin-top: 4px; width: 100%;"
				href="userspace/PubRegist.do">注册新用户</a>&nbsp;
		</PF:IfParameterEquals>
	</div>
</form>
<c:if test="${STATE=='1'}">
	<div class="text-center" id="romovealertMessageErrorId"
		style="margin: 4px; color: red; border-top: 1px dashed #ccc; padding-top: 20px;">
		<span class="glyphicon glyphicon-exclamation-sign"></span> ${MESSAGE}
	</div>
</c:if>
<div class="text-center" id="alertMessageErrorId"
	style="margin: 4px; color: red; border-top: 1px dashed #ccc; padding-top: 20px;"></div>
<script type="text/javascript">
	$(function() {
		$('#alertMessageErrorId').hide();
		$('#loginButtonId').bind('click', function() {
			preSubmitLoginForm();
		});
		$('#checkcodeimgId').bind(
				"click",
				function(e) {
					$('#checkcodeimgId').attr(
							"src",
							"webfile/Pubcheckcode.do?time="
									+ new Date().getTime());
				});
		$('#loginNameId').keydown(function(e) {
			if (e.keyCode == 13) {
				//$('#loginUrlId').val(window.location.href);
				$('#loginButtonId').click();
			}
		});
		$('#loginPassWId').keydown(function(e) {
			if (e.keyCode == 13) {
				//$('#loginUrlId').val(window.location.href);
				$('#loginButtonId').click();
			}
		});
		$('#checkcodeId').keydown(function(e) {
			if (e.keyCode == 13) {
				//$('#loginUrlId').val(window.location.href);
				$('#loginButtonId').click();
			}
		});
		$("#loginNameId").change(function() {
			initCheckCode(false);
		});
	});

	//预登陆
	function preSubmitLoginForm() {
		if ($('#loginNameId').val() && $('#loginPassWId').val()) {
			initCheckCode(function(isRequireChackCode) {
				doSubmitLoginForm(true);
			});
		} else {
			$('#alertMessageErrorId').show();
			$('#romovealertMessageErrorId').hide();
			$('#alertMessageErrorId')
					.html(
							'<span class="glyphicon glyphicon-exclamation-sign"></span>请输入用户名/密码');
		}
	}
	//初始化验证码（如果需要则显示验证码）
	function initCheckCode(isSubmit) {
		$.post('login/PubRequireCCode.do', {
			'loginname' : $('#loginNameId').val()
		}, function(flag) {
			if (flag.require) {
				$('#loginCheckFormId').show();
				if (isSubmit) {
					doSubmitLoginForm(true);
				}
			} else {
				if (isSubmit) {
					doSubmitLoginForm(false);
				}
			}
		}, 'json');
	}

	//执行登陆
	function doSubmitLoginForm(isRequireChackCode) {
		//校验验证码
		if (isRequireChackCode && !$('#checkcodeId').val()) {
			$('#alertMessageErrorId').show();
			$('#romovealertMessageErrorId').hide();
			$('#alertMessageErrorId')
					.html(
							'<span class="glyphicon glyphicon-exclamation-sign"></span>请输入验证码');
			return;
		}
		$('#loginButtonId').addClass("disabled");
		$('#loginButtonId').text("提交中...");
		var scret_password = (AuthKeyProvider.encodeLogin(
				'${config_password_provider_type}', $('#loginPassWId').val(),
				$('#loginNameId').val(), '${sck}'));
		$('#realPasswordId').val(scret_password);
		$('#loginFormId').submit();
	}
</script>