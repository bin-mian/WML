package com.farm.file.service;

import com.farm.file.domain.FileBase;
import com.farm.file.domain.Visit;
import com.farm.file.domain.Visitlogs;
import com.farm.core.sql.query.DataQuery;

import java.util.List;

import com.farm.core.auth.domain.LoginUser;


public interface VisitServiceInter {

	public enum OpModel {
		//
		VISIT("3", "访问"),
		//
		DOWN("1", "下载"),
		//
		VIEW("2", "预览"),
		//
		PRAISE("4", "评价"),
		//
		COMMENTS("5", "评论"),
		//
		FAVORITES("6", "收藏"),
		//
		BOOK("7", "订阅");

		private String type;
		private String title;

		OpModel(String type, String title) {
			this.type = type;
			this.title = title;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

	}

	
	public Visit insertFilevisitEntity(Visit entity, LoginUser user);

	
	public Visit editFilevisitEntity(Visit entity, LoginUser user);

	
	public void deleteFilevisitEntity(String id, LoginUser user);

	
	public Visit getFilevisitEntity(String id);

	
	public DataQuery createFilevisitSimpleQuery(DataQuery query);

	
	public void insertFilevisitEntity(FileBase entity, LoginUser user);

	
	public Visit getVisitByAppId(String appid);

	
	public void deleteVisitByAppid(String appid, LoginUser user);

	
	public Visit record(String appid, OpModel visit, LoginUser user);

	public Visit record(String appid, OpModel visit, LoginUser user, int val1, String val2);

	public Visit record(String appid, OpModel visit, LoginUser user, int val);

	public Visitlogs record(String appid, OpModel visit, LoginUser user, String val);

	
	public Visitlogs getRecordLog(String appid, OpModel visit, LoginUser currentUser);

	
	public List<Visitlogs> getRecordLogs(String appid, OpModel comments);

	
	public int getUserLogsNum(LoginUser currentUser, OpModel opmodel);

	
	public boolean hasVisitLog(String id, OpModel praise, LoginUser currentUser);
}