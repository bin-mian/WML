package com.farm.file.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


@Entity(name = "VisitLogs")
@Table(name = "wml_f_visitlogs")
public class Visitlogs implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "systemUUID", strategy = "uuid")
	@GeneratedValue(generator = "systemUUID")
	@Column(name = "ID", length = 32, insertable = true, updatable = true, nullable = false)
	private String id;
	@Column(name = "STRFLAG", length = 256)
	private String strflag;
	@Column(name = "INTFLAG", length = 10)
	private Integer intflag;
	@Column(name = "TYPE", length = 2, nullable = false)
	private String type;
	@Column(name = "CUSER", length = 32, nullable = false)
	private String cuser;
	@Column(name = "CTIME", length = 14, nullable = false)
	private String ctime;
	@Column(name = "APPID", length = 32, nullable = false)
	private String appid;
	@Column(name = "CUSERNAME", length = 128)
	private String cusername;
	public String getStrflag() {
		return this.strflag;
	}

	public void setStrflag(String strflag) {
		this.strflag = strflag;
	}

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getCusername() {
		return cusername;
	}

	public void setCusername(String cusername) {
		this.cusername = cusername;
	}

	public Integer getIntflag() {
		return this.intflag;
	}

	public void setIntflag(Integer intflag) {
		this.intflag = intflag;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCuser() {
		return this.cuser;
	}

	public void setCuser(String cuser) {
		this.cuser = cuser;
	}

	public String getCtime() {
		return this.ctime;
	}

	public void setCtime(String ctime) {
		this.ctime = ctime;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}
}