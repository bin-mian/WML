package com.farm.material.service.impl;

import com.farm.material.domain.Categray;
import com.farm.material.domain.Tag;
import com.farm.core.time.TimeTool;
import org.apache.log4j.Logger;

import com.farm.material.dao.CategrayDaoInter;
import com.farm.material.dao.TagDaoInter;
import com.farm.material.service.TagServiceInter;
import com.farm.util.web.FarmFormatUnits;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.query.DataQuerys;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import com.farm.core.auth.domain.LoginUser;


@Service
public class TagServiceImpl implements TagServiceInter {
	@Resource
	private TagDaoInter tagDaoImpl;
	@Resource
	private CategrayDaoInter categrayDaoImpl;
	private static final Logger log = Logger.getLogger(TagServiceImpl.class);

	@Override
	@Transactional
	public Tag insertTagEntity(Tag entity, LoginUser user) {
		entity.setCtime(TimeTool.getTimeDate14());
		entity.setCusername(user.getName());
		entity.setPstate("1");
		return tagDaoImpl.insertEntity(entity);
	}

	@Override
	@Transactional
	public Tag editTagEntity(Tag entity, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		Tag entity2 = tagDaoImpl.getEntity(entity.getId());
		// entity2.setEuser(user.getId());
		// entity2.setEusername(user.getName());
		// entity2.setEtime(TimeTool.getTimeDate14());
		entity2.setGroupname(entity.getGroupname());
		entity2.setName(entity.getName());
		entity2.setPstate(entity.getPstate());
		entity2.setPcontent(entity.getPcontent());
		// entity2.setCusername(entity.getCusername());
		// entity2.setCtime(entity.getCtime());
		entity2.setId(entity.getId());
		tagDaoImpl.editEntity(entity2);
		return entity2;
	}

	@Override
	@Transactional
	public void deleteTagEntity(String id, LoginUser user) {
		// TODO 自动生成代码,修改后请去除本注释
		tagDaoImpl.deleteEntity(tagDaoImpl.getEntity(id));
	}

	@Override
	@Transactional
	public Tag getTagEntity(String id) {
		// TODO 自动生成代码,修改后请去除本注释
		if (id == null) {
			return null;
		}
		return tagDaoImpl.getEntity(id);
	}

	@Override
	@Transactional
	public DataQuery createTagSimpleQuery(DataQuery query) {
		// TODO 自动生成代码,修改后请去除本注释
		DataQuery dbQuery = DataQuery.init(query, "WML_C_TAG", "ID,GROUPNAME,NAME,PSTATE,PCONTENT,CUSERNAME,CTIME");
		return dbQuery;
	}

	@Override
	@Transactional
	public List<Tag> getTags(String categrayId) {
		Categray categray=	categrayDaoImpl.getEntity(categrayId);
		if(categray==null) {
			return new ArrayList<Tag>();
		}
		List<String> categrays = FarmFormatUnits.SplitStringByLen(categray.getTreecode(),
				32);
		DataQuery dbQuery = DataQuery.getInstance(1,
				"a.ID as ID,a.GROUPNAME as GROUPNAME,a.NAME as NAME,a.PSTATE as PSTATE,a.PCONTENT as PCONTENT,a.CUSERNAME as CUSERNAME,a.CTIME as CTIME",
				"WML_C_TAG a left join WML_C_CATEGRAYTAG b on b.tagid=a.id");
		dbQuery.setPagesize(1000);
		dbQuery.setNoCount();
		dbQuery.setDistinct(true);
		dbQuery.addSqlRule(" and b.categrayid in (" + DataQuerys.getWhereInSubVals(categrays) + ")");
		try {
			List<Tag> lsit = dbQuery.search().getObjectList(Tag.class);
			return lsit;
		} catch (SQLException e) {
			e.printStackTrace();
			return new ArrayList<Tag>();
		}
	}

}
