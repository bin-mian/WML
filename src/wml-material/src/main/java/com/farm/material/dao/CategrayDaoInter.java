package com.farm.material.dao;

import com.farm.material.domain.Categray;
import org.hibernate.Session;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import java.util.List;
import java.util.Map;


public interface CategrayDaoInter {
	
	public void deleteEntity(Categray categray);

	
	public Categray getEntity(String categrayid);

	
	public Categray insertEntity(Categray categray);

	
	public int getAllListNum();

	
	public void editEntity(Categray categray);

	
	public Session getSession();

	
	public DataResult runSqlQuery(DataQuery query);

	
	public void deleteEntitys(List<DBRule> rules);

	
	public List<Categray> selectEntitys(List<DBRule> rules);

	
	public void updataEntitys(Map<String, Object> values, List<DBRule> rules);

	
	public int countEntitys(List<DBRule> rules);

	public List<Categray> getAllSubNodes(String typeid);

	
	public Integer getAllsubFileNum(String categrayid);
}