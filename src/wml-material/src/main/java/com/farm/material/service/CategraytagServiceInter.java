package com.farm.material.service;

import com.farm.material.domain.Categraytag;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.auth.domain.LoginUser;


public interface CategraytagServiceInter {
	
	public Categraytag insertCategraytagEntity(Categraytag entity, LoginUser user);

	
	public Categraytag editCategraytagEntity(Categraytag entity, LoginUser user);

	
	public void deleteCategraytagEntity(String id, LoginUser user);

	
	public Categraytag getCategraytagEntity(String id);

	
	public DataQuery createCategraytagSimpleQuery(DataQuery query);

	
	public void dobind(String tagids, String categrayid);
}