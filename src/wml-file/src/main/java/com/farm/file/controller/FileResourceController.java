package com.farm.file.controller;

import com.farm.file.domain.FileResource;
import com.farm.file.service.FileResourceServiceInter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.annotation.Resource;
import com.farm.web.easyui.EasyUiUtils;

import java.io.File;
import java.util.Map;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpSession;
import com.farm.core.page.RequestMode;
import com.farm.core.page.OperateType;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import com.farm.core.sql.result.ResultsHandle;
import com.farm.core.page.ViewMode;
import com.farm.web.WebUtils;


@RequestMapping("/fileresource")
@Controller
public class FileResourceController extends WebUtils {
	private final static Logger log = Logger.getLogger(FileResourceController.class);
	@Resource
	private FileResourceServiceInter fileResourceServiceImpl;

	
	@RequestMapping("/query")
	@ResponseBody
	public Map<String, Object> queryall(DataQuery query, HttpServletRequest request) {
		try {
			query = EasyUiUtils.formatGridQuery(request, query);
			DataResult result = fileResourceServiceImpl.createFileresourceSimpleQuery(query).search();
			result.runDictionary("1:读写,2:只读,0:禁用", "STATE");
			result.runformatTime("ETIME", "yyyy-MM-dd HH:mm:ss");
			result.runHandle(new ResultsHandle() {
				@Override
				public void handle(Map<String, Object> row) {
					try {
						String path = (String) row.get("PATH");
						File[] roots = File.listRoots();
						for (File rootfile : roots) {
							if (path.indexOf(rootfile.getPath()) >= 0) {
								long usable = ((rootfile.getUsableSpace() / (1024 * 1024)) / 1024);
								long total = ((rootfile.getTotalSpace() / (1024 * 1024)) / 1024);
								if (usable != 0 && total != 0) {
									row.put("SPACEINFO",
											"可用空间" + usable + "G/总空间" + total + "G(" + (usable * 100 / total) + "%)");
								}
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
						row.put("SPACEINFO", "当前环境无法计算");
					}
				}
			});
			return ViewMode.getInstance().putAttrs(EasyUiUtils.formatGridData(result)).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}
	
	
	
	

	
	@RequestMapping("/edit")
	@ResponseBody
	public Map<String, Object> editSubmit(FileResource entity, HttpSession session) {
		try {
			entity = fileResourceServiceImpl.editFileresourceEntity(entity, getCurrentUser(session));
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).putAttr("entity", entity).returnObjMode();

		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).setError(e.getMessage(), e).returnObjMode();
		}
	}

	
	@RequestMapping("/add")
	@ResponseBody
	public Map<String, Object> addSubmit(FileResource entity, HttpSession session) {
		// TODO 自动生成代码,修改后请去除本注释
		try {
			entity = fileResourceServiceImpl.insertFileresourceEntity(entity, getCurrentUser(session));
			return ViewMode.getInstance().setOperate(OperateType.ADD).putAttr("entity", entity).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.ADD).setError(e.getMessage(), e).returnObjMode();
		}
	}

	
	@RequestMapping("/del")
	@ResponseBody
	public Map<String, Object> delSubmit(String ids, HttpSession session) {
		try {
			for (String id : parseIds(ids)) {
				fileResourceServiceImpl.deleteFileresourceEntity(id, getCurrentUser(session));
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/list")
	public ModelAndView index(HttpSession session) {
		return ViewMode.getInstance().returnModelAndView("file/FileResourceResult");
	}

	
	@RequestMapping("/form")
	public ModelAndView view(RequestMode pageset, String ids) {
		try {
			switch (pageset.getOperateType()) {
			case (0): {// 查看
				return ViewMode.getInstance().putAttr("pageset", pageset)
						.putAttr("entity", fileResourceServiceImpl.getFileresourceEntity(ids))
						.returnModelAndView("file/FileResourceForm");
			}
			case (1): {// 新增
				return ViewMode.getInstance().putAttr("pageset", pageset).returnModelAndView("file/FileResourceForm");
			}
			case (2): {// 修改
				return ViewMode.getInstance().putAttr("pageset", pageset)
						.putAttr("entity", fileResourceServiceImpl.getFileresourceEntity(ids))
						.returnModelAndView("file/FileResourceForm");
			}
			default:
				break;
			}
			return ViewMode.getInstance().returnModelAndView("file/FileResourceForm");
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e + e.getMessage(), e).returnModelAndView("file/FileResourceForm");
		}
	}
}
