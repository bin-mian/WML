package com.farm.web.init;

import javax.servlet.ServletContext;

import com.farm.core.config.AppConfig;
import com.farm.wcp.util.InitFilePathUtil;
import com.farm.web.task.ServletInitJobInter;

/**展示项目信息
 * @author macpl
 *
 */
public class InitStartInfo implements ServletInitJobInter {
	@Override
	public void execute(ServletContext context) {
		InitFilePathUtil.iniPaths();
		System.out.println("physics path:" + context.getRealPath(""));
		System.out.println("wml version:" + AppConfig.getString("config.sys.version"));
	}
}
