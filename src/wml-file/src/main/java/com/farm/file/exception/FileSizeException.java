package com.farm.file.exception;


public class FileSizeException extends Exception {

	
	private static final long serialVersionUID = -2169402402793297968L;

	public FileSizeException(String message) {
		super(message);
	}

}
