<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<div class="easyui-layout" data-options="fit:true">
	<div data-options="region:'north',border:false">
		<form id="searchParareadyparaForm">
			<table class="editTable">
				<tr style="text-align: center;">
					<td class="title">参数名称:</td>
					<td><input name="B.NAME:like" type="text"></td>
					<td class="title">组名称:</td>
					<td><input name="C.NAME:like" type="text"></td>
					<td><a id="a_search" href="javascript:void(0)"
						class="easyui-linkbutton" iconCls="icon-search">查询</a> <a
						id="a_reset" href="javascript:void(0)" class="easyui-linkbutton"
						iconCls="icon-reload">清除条件</a></td>
				</tr>
			</table>
		</form>
	</div>
	<div data-options="region:'center',border:false">
		<table id="dataParareadyparaGrid">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th field="NAME" data-options="sortable:true" width="40">名称</th>
					<th field="GROUPNAME" data-options="sortable:true" width="40">组名称</th>
					<th field="PKEY" data-options="sortable:true" width="40">PKEY</th>
					<th field="PVAL" data-options="sortable:true" width="50">参数值</th>
					<th field="STATE" data-options="sortable:true" width="20">状态</th>
				</tr>
			</thead>
		</table>
	</div>
</div>
<script type="text/javascript">
	var url_delActionParareadypara = "parareadypara/del.do";//删除URL
	var url_formActionParareadypara = "parareadypara/form.do";//增加、修改、查看URL
	var url_searchActionParareadypara = "parareadypara/query.do?sysid=${sysid}";//查询URL
	var title_windowParareadypara = "预加载参数管理";//功能名称
	var gridParareadypara;//数据表格对象
	var searchParareadypara;//条件查询组件对象
	var toolBarParareadypara = [
	{
		id : 'add',
		text : '导入参数',
		iconCls : 'icon-add',
		handler : addDataPara
	}, {
		id : 'add',
		text : '导入参数组',
		iconCls : 'icon-add',
		handler : addDataParaGroup
	}, {
		id : 'edit',
		text : '设置参数',
		iconCls : 'icon-edit',
		handler : editDataParareadypara
	}, {
		id : 'del',
		text : '移除参数',
		iconCls : 'icon-remove',
		handler : delDataParareadypara
	},{
		id : 'view',
		text : '查看备注',
		iconCls : 'icon-tip',
		handler : viewDataParareadypara
	}, {
		id : 'syncPras',
		text : '下发参数',
		iconCls : 'icon-communication',
		handler : sycnParas
	} ];
	$(function() {
		//初始化数据表格
		gridParareadypara = $('#dataParareadyparaGrid').datagrid({
			url : url_searchActionParareadypara,
			fit : true,
			fitColumns : true,
			'toolbar' : toolBarParareadypara,
			pagination : true,
			closable : true,
			checkOnSelect : true,
			border : false,
			striped : true,
			rownumbers : true,
			ctrlSelect : true
		});
		//初始化条件查询
		searchParareadypara = $('#searchParareadyparaForm').searchForm({
			gridObj : gridParareadypara
		});
	});
	//查看
	function viewDataParareadypara() {
		var selectedArray = $(gridParareadypara).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionParareadypara + '?pageset.pageType='
					+ PAGETYPE.VIEW + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winParareadypara',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '浏览'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//新增组
	function addDataParaGroup() {
		chooseWindowCallBackHandle = function(rows) {
			var ids = "";
			$(rows).each(function(i, obj) {
				ids = ids + "," + obj.ID;
			});
			$.post('subsys/addParaGroups.do?sysid=${sysid}&groupids=' + ids,
					{}, function(flag) {
						var jsonObject = JSON.parse(flag, null);
						$(gridParareadypara).datagrid('loaded');
						if (jsonObject.STATE == 0) {
							$(gridParareadypara).datagrid('reload');
							$("#chooseParagroupWin").window('close');
						} else {
							var str = MESSAGE_PLAT.ERROR_SUBMIT
									+ jsonObject.MESSAGE;
							$.messager.alert(MESSAGE_PLAT.ERROR, str, 'error');
						}
					});
		};
		$.farm.openWindow({
			id : 'chooseParagroupWin',
			width : 700,
			height : 400,
			modal : true,
			url : 'paragroup/ParagroupChooseGridPage.do',
			title : '参数组'
		});
	}

	//新增
	function addDataPara() {
		chooseWindowCallBackHandle = function(rows) {
			var ids = "";
			$(rows).each(function(i, obj) {
				ids = ids + "," + obj.ID;
			});
			$.post('subsys/addParas.do?sysid=${sysid}&paraDefineids=' + ids,
					{}, function(flag) {
						var jsonObject = JSON.parse(flag, null);
						$(gridParareadypara).datagrid('loaded');
						if (jsonObject.STATE == 0) {
							$(gridParareadypara).datagrid('reload');
							$("#chooseParadefineWin").window('close');
						} else {
							var str = MESSAGE_PLAT.ERROR_SUBMIT
									+ jsonObject.MESSAGE;
							$.messager.alert(MESSAGE_PLAT.ERROR, str, 'error');
						}
					});
		};
		$.farm.openWindow({
			id : 'chooseParadefineWin',
			width : 700,
			height : 400,
			modal : true,
			url : 'paradefine/ParadefineChooseGridPage.do',
			title : '参数定义'
		});
	}
	//修改
	function editDataParareadypara() {
		var selectedArray = $(gridParareadypara).datagrid('getSelections');
		if (selectedArray.length == 1) {
			var url = url_formActionParareadypara + '?operateType='
					+ PAGETYPE.EDIT + '&ids=' + selectedArray[0].ID;
			$.farm.openWindow({
				id : 'winParareadypara',
				width : 600,
				height : 300,
				modal : true,
				url : url,
				title : '修改'
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE_ONLY,
					'info');
		}
	}
	//删除
	function delDataParareadypara() {
		var selectedArray = $(gridParareadypara).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + MESSAGE_PLAT.SUCCESS_DEL_NEXT_IS;
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridParareadypara).datagrid('loading');
					$.post(url_delActionParareadypara + '?ids='
							+ $.farm.getCheckedIds(gridParareadypara, 'ID'),
							{}, function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridParareadypara).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridParareadypara).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}

	//同步自定义参数
	function sycnParas() {
		var selectedArray = $(gridParareadypara).datagrid('getSelections');
		if (selectedArray.length > 0) {
			// 有数据执行操作
			var str = selectedArray.length + "条参数将被同步，是否继续?";
			$.messager.confirm(MESSAGE_PLAT.PROMPT, str, function(flag) {
				if (flag) {
					$(gridParareadypara).datagrid('loading');
					$.post('subsys/doSyncDefineParas.do?readyparaIds='
							+ $.farm.getCheckedIds(gridParareadypara, 'ID'),
							{}, function(flag) {
								var jsonObject = JSON.parse(flag, null);
								$(gridParareadypara).datagrid('loaded');
								if (jsonObject.STATE == 0) {
									$(gridParareadypara).datagrid('reload');
								} else {
									var str = MESSAGE_PLAT.ERROR_SUBMIT
											+ jsonObject.MESSAGE;
									$.messager.alert(MESSAGE_PLAT.ERROR, str,
											'error');
								}
							});
				}
			});
		} else {
			$.messager.alert(MESSAGE_PLAT.PROMPT, MESSAGE_PLAT.CHOOSE_ONE,
					'info');
		}
	}
</script>