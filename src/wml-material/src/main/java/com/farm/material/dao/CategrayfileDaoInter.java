package com.farm.material.dao;

import com.farm.material.domain.Categrayfile;
import org.hibernate.Session;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import java.util.List;
import java.util.Map;



public interface CategrayfileDaoInter  {
 
 public void deleteEntity(Categrayfile categrayfile) ;
 
 public Categrayfile getEntity(String categrayfileid) ;
 
 public  Categrayfile insertEntity(Categrayfile categrayfile);
 
 public int getAllListNum();
 
 public void editEntity(Categrayfile categrayfile);
 
 public Session getSession();
 
 public DataResult runSqlQuery(DataQuery query);
 
 public void deleteEntitys(List<DBRule> rules);

 
 public List<Categrayfile> selectEntitys(List<DBRule> rules);

 
 public void updataEntitys(Map<String, Object> values, List<DBRule> rules);
 
 public int countEntitys(List<DBRule> rules);
}