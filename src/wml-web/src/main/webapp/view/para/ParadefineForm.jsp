<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!--参数定义表单-->
<div class="easyui-layout" data-options="fit:true">
	<div class="TableTitle" data-options="region:'north',border:false">
		<span class="label label-primary"> <c:if
				test="${pageset.operateType==1}">新增${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==2}">修改${JSP_Messager_Title}记录</c:if> <c:if
				test="${pageset.operateType==0}">浏览${JSP_Messager_Title}记录</c:if>
		</span>
	</div>
	<div data-options="region:'center'">
		<form id="dom_formParadefine">
			<input type="hidden" id="entity_id" name="id" value="${entity.id}">
			<table class="editTable">
				<tr>
					<td class="title">名称:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[64]']"
						id="entity_name" name="name" value="${entity.name}"></td>
				</tr>
				<tr>
					<td class="title">PKEY:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[64]']"
						id="entity_pkey" name="pkey" value="${entity.pkey}"></td>
				</tr>
				<tr>
					<td class="title">注释:</td>
					<td colspan="3"><textarea rows="2" style="width: 360px;"
							class="easyui-validatebox"
							data-options="validType:[,'maxLength[256]']" id="entity_note"
							name="note">${entity.note}</textarea></td>
				</tr>
				<!-- <tr>
					<td class="title">PCONTENT:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="validType:[,'maxLength[64]']" id="entity_pcontent"
						name="pcontent" value="${entity.pcontent}"></td>
				</tr>
				<tr>
					<td class="title">CTIME:</td>
					<td colspan="3"><input type="text" style="width: 360px;"
						class="easyui-validatebox"
						data-options="required:true,validType:[,'maxLength[8]']"
						id="entity_ctime" name="ctime" value="${entity.ctime}"></td>
				</tr> -->
			</table>
		</form>
	</div>
	<div data-options="region:'south',border:false">
		<div class="div_button" style="text-align: center; padding: 4px;">
			<c:if test="${pageset.operateType==1}">
				<a id="dom_add_entityParadefine" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">增加</a>
			</c:if>
			<c:if test="${pageset.operateType==2}">
				<a id="dom_edit_entityParadefine" href="javascript:void(0)"
					iconCls="icon-save" class="easyui-linkbutton">修改</a>
			</c:if>
			<a id="dom_cancle_formParadefine" href="javascript:void(0)"
				iconCls="icon-cancel" class="easyui-linkbutton"
				style="color: #000000;">取消</a>
		</div>
	</div>
</div>
<script type="text/javascript">
	var submitAddActionParadefine = 'paradefine/add.do';
	var submitEditActionParadefine = 'paradefine/edit.do';
	var currentPageTypeParadefine = '${pageset.operateType}';
	var submitFormParadefine;
	$(function() {
		//表单组件对象
		submitFormParadefine = $('#dom_formParadefine').SubmitForm({
			pageType : currentPageTypeParadefine,
			grid : gridParadefine,
			currentWindowId : 'winParadefine'
		});
		//关闭窗口
		$('#dom_cancle_formParadefine').bind('click', function() {
			$('#winParadefine').window('close');
		});
		//提交新增数据
		$('#dom_add_entityParadefine').bind('click', function() {
			submitFormParadefine.postSubmit(submitAddActionParadefine);
		});
		//提交修改数据
		$('#dom_edit_entityParadefine').bind('click', function() {
			submitFormParadefine.postSubmit(submitEditActionParadefine);
		});
	});
//-->
</script>