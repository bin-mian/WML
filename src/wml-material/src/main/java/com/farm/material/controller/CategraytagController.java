package com.farm.material.controller;

import com.farm.material.domain.Categraytag;
import com.farm.material.service.CategraytagServiceInter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.annotation.Resource;
import com.farm.web.easyui.EasyUiUtils;
import java.util.Map;
import java.util.function.Consumer;

import org.apache.log4j.Logger;
import org.apache.poi.ss.formula.functions.T;

import javax.servlet.http.HttpSession;
import com.farm.core.page.RequestMode;
import com.farm.core.page.OperateType;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.query.DataQuerys;
import com.farm.core.sql.result.DataResult;
import com.farm.core.page.ViewMode;
import com.farm.web.WebUtils;


@RequestMapping("/categraytag")
@Controller
public class CategraytagController extends WebUtils {
	private final static Logger log = Logger.getLogger(CategraytagController.class);
	@Resource
	private CategraytagServiceInter categrayTagServiceImpl;

	
	@RequestMapping("/query")
	@ResponseBody
	public Map<String, Object> queryall(DataQuery query, String tagids, HttpServletRequest request) {
		try {
			query = EasyUiUtils.formatGridQuery(request, query);
			query.addSqlRule("and TAGID in (" + DataQuerys.getWhereInSubVals(WebUtils.parseIds(tagids)) + ")");
			DataResult result = categrayTagServiceImpl.createCategraytagSimpleQuery(query).search();
			return ViewMode.getInstance().putAttrs(EasyUiUtils.formatGridData(result)).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	
	@RequestMapping("/edit")
	@ResponseBody
	public Map<String, Object> editSubmit(Categraytag entity, HttpSession session) {
		// TODO 自动生成代码,修改后请去除本注释
		try {
			entity = categrayTagServiceImpl.editCategraytagEntity(entity, getCurrentUser(session));
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).putAttr("entity", entity).returnObjMode();

		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).setError(e.getMessage(), e).returnObjMode();
		}
	}

	
	@RequestMapping("/dobind")
	@ResponseBody
	public Map<String, Object> dobind(String tagids, String categrayid, HttpSession session) {
		try { 
			WebUtils.parseIds(tagids).stream().forEach((tagid) -> categrayTagServiceImpl.dobind(tagid, categrayid));
			return ViewMode.getInstance().setOperate(OperateType.ADD).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.ADD).setError(e.getMessage(), e).returnObjMode();
		}
	}

	
	@RequestMapping("/add")
	@ResponseBody
	public Map<String, Object> addSubmit(Categraytag entity, HttpSession session) {
		// TODO 自动生成代码,修改后请去除本注释
		try {
			entity = categrayTagServiceImpl.insertCategraytagEntity(entity, getCurrentUser(session));
			return ViewMode.getInstance().setOperate(OperateType.ADD).putAttr("entity", entity).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.ADD).setError(e.getMessage(), e).returnObjMode();
		}
	}

	
	@RequestMapping("/del")
	@ResponseBody
	public Map<String, Object> delSubmit(String ids, HttpSession session) {
		try {
			for (String id : parseIds(ids)) {
				categrayTagServiceImpl.deleteCategraytagEntity(id, getCurrentUser(session));
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/list")
	public ModelAndView index(HttpSession session, String ids) {
		return ViewMode.getInstance().putAttr("tagids", ids).returnModelAndView("material/CategraytagResult");
	}

	
	@RequestMapping("/form")
	public ModelAndView view(RequestMode pageset, String ids) {
		try {
			switch (pageset.getOperateType()) {
			case (0): {// 查看
				return ViewMode.getInstance().putAttr("pageset", pageset)
						.putAttr("entity", categrayTagServiceImpl.getCategraytagEntity(ids))
						.returnModelAndView("material/CategraytagForm");
			}
			case (1): {// 新增
				return ViewMode.getInstance().putAttr("pageset", pageset)
						.returnModelAndView("material/CategraytagForm");
			}
			case (2): {// 修改
				return ViewMode.getInstance().putAttr("pageset", pageset)
						.putAttr("entity", categrayTagServiceImpl.getCategraytagEntity(ids))
						.returnModelAndView("material/CategraytagForm");
			}
			default:
				break;
			}
			return ViewMode.getInstance().returnModelAndView("material/CategraytagForm");
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e + e.getMessage(), e)
					.returnModelAndView("material/CategraytagForm");
		}
	}
}
