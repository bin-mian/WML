package com.farm.wcp.util;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import com.farm.core.auth.exception.CheckCodeErrorException;
import com.farm.parameter.FarmParameterService;

/**
 * 校验登录验证码工具类
 * 
 * @author lenovo
 *
 */
public class CheckCodeUtil {

	/**
	 * 用户登陆名登陆失败次数
	 */
	private static Map<String, Integer> LOGIN_ERROR_NUM = new HashMap<String, Integer>();

	/**
	 * 判断验证码是否能通过验证(不启用验证码时为可以通过验证)
	 * 
	 * @param session
	 * @return
	 * @throws CheckCodeErrorException
	 */
	public static boolean isCheckCodeAble(String loginname, String checkcode, HttpSession session)
			throws CheckCodeErrorException {
		if (isRequireCheckCode(loginname, session)) {
			// 需要验证码
			boolean ischeck = checkcode != null && checkcode.toUpperCase().equals(session.getAttribute("verCode"));
			session.removeAttribute("verCode");
			if (!ischeck) {
				// 验证码错误
				throw new CheckCodeErrorException("验证码错误");
			} else {
				// 验证码输入正确
				//當前用戶錄入錯誤次數超過規定次數后会启用所有验证码强制校验逻辑，所以只有成功录入验证码后才会取消
				session.setAttribute("CHECK_CODE_REQUIRE_IS", "FALSE");
			}
		}
		return true;
	}

	/**
	 * 增加用户登陆失败次数
	 * 
	 * @param checkcode
	 * @param session
	 */
	public static void addLoginErrorNum(String loginname) {
		Integer errornum = LOGIN_ERROR_NUM.get(loginname);
		if (errornum == null) {
			errornum = 1;
		} else {
			errornum++;
		}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
		if (LOGIN_ERROR_NUM.size() > 1000) {
			LOGIN_ERROR_NUM.clear();
		}
		LOGIN_ERROR_NUM.put(loginname, errornum);
	}

	/**
	 * 免验证码登录次数复位
	 * 
	 * @param checkcode
	 * @param session
	 */
	public static void setLoginSuccess(String loginname) {
		if (LOGIN_ERROR_NUM.size() > 1000) {
			LOGIN_ERROR_NUM.clear();
		}
		LOGIN_ERROR_NUM.put(loginname.trim(), 0);
	}

	/**
	 * 是否需要验证码
	 * 
	 * @param loginname
	 * @return
	 */
	public static boolean isRequireCheckCode(String loginname, HttpSession session) {
		if (FarmParameterService.getInstance().getParameter("config.sys.verifycode.able").equals("true")) {
			if (session.getAttribute("CHECK_CODE_REQUIRE_IS") != null
					&& session.getAttribute("CHECK_CODE_REQUIRE_IS").equals("TRUE")) {
				// 当前session必须验证码
				return true;
			}
			// 用户名最大允许错误次数
			int errorMaxNum = FarmParameterService.getInstance().getParameterInt("config.sys.verifycode.checknum");
			// 用户名当前错误次数
			Integer errornum = LOGIN_ERROR_NUM.get(loginname) == null ? 0 : LOGIN_ERROR_NUM.get(loginname);
			// 用戶名達到錯誤次数后开启验证码
			if (errornum >= errorMaxNum) {
				session.setAttribute("CHECK_CODE_REQUIRE_IS", "TRUE");
				return true;
			}
		}
		return false;
	}

	/**
	 * 强制当前session使用验证码
	 * 
	 * @param session
	 */
	public static void setRequireCheckCode(HttpSession session) {
		session.setAttribute("CHECK_CODE_REQUIRE_IS", "TRUE");
	}

	/**
	 * 设置当前session的验证码
	 * 
	 * @param verifyCode
	 * @param session
	 */
	public static void resetCheckCode(String verifyCode, HttpSession session) {
		// 删除以前的
		session.removeAttribute("verCode");
		session.setAttribute("verCode", verifyCode.toUpperCase());
	}

	public static String refreshCheckCode(HttpSession session) {
		// 生成随机字串
		String verifyCode = VerifyCodeUtils.generateVerifyCode(4);
		// 删除以前的
		session.removeAttribute("verCode");
		session.setAttribute("verCode", verifyCode.toUpperCase());
		return verifyCode;
	}
}
