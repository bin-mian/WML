<%@ page language="java" pageEncoding="utf-8"%>
<%@page import="com.farm.web.constant.FarmConstant"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/view/conf/farmtag.tld" prefix="PF"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<base href="<PF:basePath/>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>用户登录-<PF:ParameterValue key="config.sys.title" /></title>
<meta name="description"
	content='<PF:ParameterValue key="config.sys.mate.description"/>'>
<meta name="keywords"
	content='<PF:ParameterValue key="config.sys.mate.keywords"/>'>
<meta name="author"
	content='<PF:ParameterValue key="config.sys.mate.author"/>'>
<meta name="robots" content="noindex,nofllow">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<jsp:include page="atext/include-web.jsp"></jsp:include>
<script src="text/javascript/base64.js"></script>
<script src="text/javascript/md5.js"></script>
<script src="text/javascript/encode.provider.js"></script>
<script src="text/javascript/jssha256.js"></script>
<style type="text/css">
.wuc-other-logins img {
	width: 32px;
	height: 32px;
	margin: 20px;
	margin-top: 0px;
	cursor: pointer;
}

.wuc-other-logins img:HOVER {
	background-color: #ffffff;
	padding: 4px;
}
</style>
</head>
<body>
	<script type="text/javascript">
		if (window != top) {
			$("#loginId").html(
					'<div style="font-size:25px;text-align:center;">'
							+ "无法在iframe中实现登录操作，正在跳转中。。。" + '<//div>');
			top.location.href = "login/webPage.html";
		}
	</script>
	<jsp:include page="commons/head.jsp"></jsp:include>
	<div class="containerbox" style="margin-top: 100px;">
		<div class="container ">
			<c:if test="${!empty outuserid }">
				<div style="text-align: center;">
					本次登陆将用户绑定到外部账户<b>${outusername }</b>
				</div>
			</c:if>
			<div></div>
			<div
				style="box-shadow: 0px 0px 50px 15px rgba(0, 0, 0, 0.1); margin: auto; border-radius: 6px 6px 6px 6px; max-width: 800px; border: 1px solid #e7e7e7; margin-top: 20px; margin-bottom: 20px; background-color: #fcfcfc;">
				<div class="panel-body" style="padding: 0px;">
					<div class="row">
						<div class="col-md-6"
							style="padding: 50px; padding-top: 10px; padding-bottom: 20px;">
							<c:if test="${USEROBJ!=null}">
								<div>
									<div
										style="text-align: center; margin-top: 50px; margin-bottom: 50px;">
										已登录&nbsp;<b>${subsys.title }</b><br /> <br /> <img alt=""
											src="download/PubPhoto.do?id=${USEROBJ.imgid}"
											style="width: 64px; height: 64px; position: relative; top: -2px;"
											class="img-circle"><br /> <br /> <b>${USEROBJ.name}</b>
									</div>
									<a class="btn btn-success text-left"
										href="login/back.do?sysid=${sysid}"
										style="margin-top: 4px; width: 100%;" type="button">进入系統</a><br />
									<br /> <a class="btn btn-danger text-left"
										href="login/webout.do?sysid=${sysid}"
										style="margin-top: 4px; width: 100%;" type="button">注销用户</a>
								</div>
							</c:if>
							<c:if test="${USEROBJ==null}">
								<h3 style="color: #666; font-size: 16px;">
									<b><PF:ParameterValue key="config.sys.title" /></b> <span
										style="white-space: nowrap;">密码-登陆</span>
								</h3>
								<hr />
								<jsp:include page="login/loginbox-password.jsp"></jsp:include>
							</c:if>
						</div>
						<div class="col-md-6 hidden-xs hidden-sm "
							style="border-left: 1px solid #e7e7e7; padding-left: 0px;">
							<div
								style="height: 450px; overflow: hidden; border-radius: 0px 6px 6px 0px; background-color: #000000; text-align: center;">
								<c:if test="${empty  sysMaxlogo}">
									<img alt="" height="450px" src="text/img/demo/maximg.jpg">
								</c:if>
								<c:if test="${!empty  sysMaxlogo}">
									<img alt="" height="450px" src="${sysMaxlogo}">
								</c:if>
							</div>
						</div>
					</div>
				</div>
			</div>
			<c:if test="${!empty logintypes}">
				<div
					style="margin-bottom: 100px; margin: auto; text-align: center; color: #999999; max-width: 800px;">
					<div class="row hidden-xs hidden-sm "
						style="padding-top: 8px; margin-bottom: 8px;">
						<div class="col-md-5"
							style="padding-left: 0px; padding-left: 18px;">
							<hr style="border-top: 1px solid #cccccc; margin: auto;">
						</div>
						<div class="col-md-2 " style="position: relative; top: -10px;">其它登陆方式</div>
						<div class="col-md-5 ">
							<hr style="border-top: 1px solid #cccccc; margin: auto;">
						</div>
					</div>
					<div class="wuc-other-logins">
						<c:forEach items="${logintypes}" var="node">
							<c:if test="${node.key=='PASSWORD'}">
								<img class="img-circle" alt="" title="密码登陆"
									src="view/web-simple/login/logo/password.png"
									onclick="gotoLoginPage('PASSWORD','${subsys.id}')">
							</c:if>
							<c:if test="${node.key=='TESTRANDOM'}">
								<img class="img-circle" alt="" title="随机用户登陆"
									src="view/web-simple/login/logo/random.png"
									onclick="gotoLoginPage('TESTRANDOM','${subsys.id}')">
							</c:if>
							<c:if test="${node.key=='WEIXIN_OAUTH2'}">
								<img class="img-circle" alt="" title="企业微信授权登陆"
									src="view/web-simple/login/logo/weixin.png"
									onclick="gotoLoginPage('WEIXIN_OAUTH2','${subsys.id}')">
							</c:if>
							<c:if test="${node.key=='WEIXIN_QRCODE'}">
								<img class="img-circle" alt="" title="企业微信扫码登陆"
									src="view/web-simple/login/logo/weixin-qr.png"
									onclick="gotoLoginPage('WEIXIN_QRCODE','${subsys.id}')">
							</c:if>
							<c:if test="${node.key=='QQ_OAUTH2'}">
								<img class="img-circle" alt="" title="QQ授权登陆"
									src="view/web-simple/login/logo/qq.png"
									onclick="gotoLoginPage('QQ_OAUTH2','${subsys.id}')">
							</c:if>
							<c:if test="${node.key=='DINGDING_OAUTH2'}">
								<img class="img-circle" alt="" title="钉钉授权登陆"
									src="view/web-simple/login/logo/dingd.png"
									onclick="gotoLoginPage('DINGDING_OAUTH2','${subsys.id}')">
							</c:if>
							<c:if test="${node.key=='DINGDING_QRCODE'}">
								<img class="img-circle" alt="" title="钉钉扫码登陆"
									src="view/web-simple/login/logo/dingd-qr.png"
									onclick="gotoLoginPage('DINGDING_QRCODE','${subsys.id}')">
							</c:if>
						</c:forEach>
					</div>
				</div>
			</c:if>
		</div>
	</div>
	<div class="container-fluid " style="padding: 0px; margin-top: 20px;">
		<jsp:include page="commons/foot.jsp"></jsp:include></div>
</body>
<script type="text/javascript">
	$(function() {
		if ($('#wml-ui-center-id').attr('id')) {
			location.href = '<PF:defaultIndexPage/>';
		}
	});
	function gotoLoginPage(type, sysid) {
		window.location.href = "<PF:basePath/>login/PubLogin.do?type=" + type
				+ "&sysid=" + sysid;
	}
</script>
</html>