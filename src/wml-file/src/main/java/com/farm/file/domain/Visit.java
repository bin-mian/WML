package com.farm.file.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;


@Entity(name = "Visit")
@Table(name = "wml_f_visit")
public class Visit implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GenericGenerator(name = "systemUUID", strategy = "uuid")
	@GeneratedValue(generator = "systemUUID")
	@Column(name = "ID", length = 32, insertable = true, updatable = true, nullable = false)
	private String id;
	@Column(name = "APPID", length = 32, nullable = false)
	private String appid;
	@Column(name = "VIEWNUM", length = 10, nullable = false)
	private Integer viewnum;
	@Column(name = "DOWNUM", length = 10, nullable = false)
	private Integer downum;
	@Column(name = "VISIT", length = 10)
	private Integer visit;
	@Column(name = "PRAISE", length = 10)
	private Integer praise;
	@Column(name = "COMMENTS", length = 10)
	private Integer comments;
	@Column(name = "FAVORITES", length = 10)
	private Integer favorites;
	@Transient
	private Visitlogs log;
	public Visitlogs getLog() {
		return log;
	}

	public void setLog(Visitlogs log) {
		this.log = log;
	}

	public Integer getVisit() {
		return visit;
	}

	public void setVisit(Integer visit) {
		this.visit = visit;
	}

	public Integer getPraise() {
		return praise;
	}

	public void setPraise(Integer praise) {
		this.praise = praise;
	}

	public Integer getComments() {
		return comments;
	}

	public void setComments(Integer comments) {
		this.comments = comments;
	}

	public Integer getFavorites() {
		return favorites;
	}

	public void setFavorites(Integer favorites) {
		this.favorites = favorites;
	}

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public Integer getViewnum() {
		return this.viewnum;
	}

	public void setViewnum(Integer viewnum) {
		this.viewnum = viewnum;
	}

	public Integer getDownum() {
		return this.downum;
	}

	public void setDownum(Integer downum) {
		this.downum = downum;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}
}