package com.farm.material.controller;

import com.farm.material.domain.Categray;
import com.farm.material.service.CategrayServiceInter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.annotation.Resource;

import com.farm.web.easyui.EasyUiTreeNode;
import com.farm.web.easyui.EasyUiUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpSession;
import com.farm.core.page.RequestMode;
import com.farm.core.page.OperateType;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import com.farm.core.page.ViewMode;
import com.farm.web.WebUtils;


@RequestMapping("/categray")
@Controller
public class CategrayController extends WebUtils {
	private final static Logger log = Logger.getLogger(CategrayController.class);
	@Resource
	private CategrayServiceInter categrayServiceImpl;

	
	@RequestMapping("/treeNodeTreeView")
	public ModelAndView forSend() {
		return ViewMode.getInstance().returnModelAndView("material/CategrayChooseTree");
	}

	
	@RequestMapping("/moveTreeNodeSubmit")
	@ResponseBody
	public Map<String, Object> moveTreeNodeSubmit(String ids, String id, HttpSession session) {
		try {
			categrayServiceImpl.moveTreeNode(ids, id, getCurrentUser(session));
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	
	@RequestMapping("/categrayTree")
	@ResponseBody
	public Object examtypeTree(String id) {
		try {
			if (id == null) {
				// 如果是未传入id，就是根节点，就构造一个虚拟的上级节点
				id = "NONE";
				List<EasyUiTreeNode> list = new ArrayList<>();
				EasyUiTreeNode nodes = new EasyUiTreeNode("NONE", "素材分类", "open", "icon-customers");
				nodes.setChildren(EasyUiTreeNode.formatAsyncAjaxTree(
						EasyUiTreeNode.queryTreeNodeOne(id, "SORT", "WML_C_CATEGRAY", "ID", "PARENTID", "NAME", "CTIME")
								.getResultList(),
						EasyUiTreeNode.queryTreeNodeTow(id, "SORT", "WML_C_CATEGRAY", "ID", "PARENTID", "NAME", "CTIME")
								.getResultList(),
						"PARENTID", "ID", "NAME", "CTIME"));
				list.add(nodes);
				return list;
			}
			return EasyUiTreeNode.formatAsyncAjaxTree(
					EasyUiTreeNode.queryTreeNodeOne(id, "SORT", "WML_C_CATEGRAY", "ID", "PARENTID", "NAME", "CTIME")
							.getResultList(),
					EasyUiTreeNode.queryTreeNodeTow(id, "SORT", "WML_C_CATEGRAY", "ID", "PARENTID", "NAME", "CTIME")
							.getResultList(),
					"PARENTID", "ID", "NAME", "CTIME");
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	
	@RequestMapping("/query")
	@ResponseBody
	public Map<String, Object> queryall(DataQuery query, HttpServletRequest request) {
		// TODO 自动生成代码,修改后请去除本注释
		try {
			query = EasyUiUtils.formatGridQuery(request, query);
			DataResult result = categrayServiceImpl.createCategraySimpleQuery(query).search();
			result.runDictionary("1:可用,0:禁用", "PSTATE");
			return ViewMode.getInstance().putAttrs(EasyUiUtils.formatGridData(result)).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	
	@RequestMapping("/edit")
	@ResponseBody
	public Map<String, Object> editSubmit(Categray entity, HttpSession session) {
		// TODO 自动生成代码,修改后请去除本注释
		try {
			entity = categrayServiceImpl.editCategrayEntity(entity, getCurrentUser(session));
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).putAttr("entity", entity).returnObjMode();

		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.UPDATE).setError(e.getMessage(), e).returnObjMode();
		}
	}

	
	@RequestMapping("/add")
	@ResponseBody
	public Map<String, Object> addSubmit(Categray entity, HttpSession session) {
		try {
			List<String> names = parseIds(entity.getName());
			int sort = entity.getSort();
			for (String name : names) {
				if (name.trim().length() > 32) {
					throw new RuntimeException("分类名称不能超过32个字符!");
				}
			}
			for (String name : names) {
				entity.setName(name.trim());
				entity.setSort(sort++);
				entity = categrayServiceImpl.insertCategrayEntity(entity, getCurrentUser(session));
			}
			return ViewMode.getInstance().setOperate(OperateType.ADD).putAttr("entity", entity).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.ADD).setError(e.getMessage(), e).returnObjMode();
		}
	}

	
	@RequestMapping("/del")
	@ResponseBody
	public Map<String, Object> delSubmit(String ids, HttpSession session) {
		try {
			for (String id : parseIds(ids)) {
				categrayServiceImpl.deleteCategrayEntity(id, getCurrentUser(session));
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	@RequestMapping("/list")
	public ModelAndView index(HttpSession session) {
		return ViewMode.getInstance().returnModelAndView("material/CategrayResult");
	}

	
	@RequestMapping("/form")
	public ModelAndView view(RequestMode pageset, String ids, String parentId) {
		try {
			switch (pageset.getOperateType()) {
			case (0): {// 查看
				return ViewMode.getInstance().putAttr("pageset", pageset)
						.putAttr("entity", categrayServiceImpl.getCategrayEntity(ids))
						.returnModelAndView("material/CategrayForm");
			}
			case (1): {// 新增
				Categray parent = null;
				if (StringUtils.isNotBlank(parentId)) {
					parent = categrayServiceImpl.getCategrayEntity(parentId);
				}
				return ViewMode.getInstance().putAttr("pageset", pageset).putAttr("parent", parent)
						.returnModelAndView("material/CategrayForm");
			}
			case (2): {// 修改
				Categray type = categrayServiceImpl.getCategrayEntity(ids);
				Categray parent = null;
				if (StringUtils.isNotBlank(type.getParentid())) {
					parent = categrayServiceImpl.getCategrayEntity(type.getParentid());
				}
				return ViewMode.getInstance().putAttr("pageset", pageset).putAttr("parent", parent)
						.putAttr("entity", categrayServiceImpl.getCategrayEntity(ids))
						.returnModelAndView("material/CategrayForm");
			}
			default:
				break;
			}
			return ViewMode.getInstance().returnModelAndView("material/CategrayForm");
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e + e.getMessage(), e).returnModelAndView("material/CategrayForm");
		}
	}
}
