package com.farm.web.tag;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.log4j.Logger;

import com.farm.parameter.FarmParameterService;

public class BasePathTaget extends TagSupport {

	
	private static final long serialVersionUID = 1L;
	static final Logger log = Logger.getLogger(BasePathTaget.class);

	@Override
	public int doEndTag() throws JspException {
		return 0;
	}

	@Override
	public int doStartTag() throws JspException {
		HttpServletRequest request = (HttpServletRequest) this.pageContext.getRequest();
		String path = request.getContextPath();
		boolean isFull = FarmParameterService.getInstance().getParameterBoolean("config.sys.basepath.full.able");
		String basePath = null;
		if (isFull) {
			basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path	+ "/";
		} else {
			basePath = path + "/";
		}
		JspWriter jspw = this.pageContext.getOut();
		try {
			jspw.print(basePath);
		} catch (IOException e) {
			log.error(e.getMessage());
		}
		return 0;
	}
}
