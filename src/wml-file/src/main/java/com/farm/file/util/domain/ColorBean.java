package com.farm.file.util.domain;

import java.awt.Color;

public class ColorBean {
	
	private String colorChName;
	
	private String colorHex;
	
	private int r;
	
	private int g;
	
	private int b;

	public ColorBean(String colorName, String colorHex) {
		Color newColor = Color.decode(colorHex);
		this.r = newColor.getRed();
		this.g = newColor.getGreen();
		this.b = newColor.getBlue();
		this.colorChName = colorName.trim();
		this.colorHex = colorHex.trim();
	}

	public String getColorChName() {
		return colorChName;
	}

	public void setColorChName(String colorChName) {
		this.colorChName = colorChName;
	}

	public String getColorHex() {
		return colorHex;
	}

	public void setColorHex(String colorHex) {
		this.colorHex = colorHex;
	}

	public int getR() {
		return r;
	}

	public void setR(int r) {
		this.r = r;
	}

	public int getG() {
		return g;
	}

	public void setG(int g) {
		this.g = g;
	}

	public int getB() {
		return b;
	}

	public void setB(int b) {
		this.b = b;
	}

	public String toString() {
		return colorChName + "(" + r + " ," + g + " ," + b + ")";
	}
}
