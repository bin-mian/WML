package com.farm.authority.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.farm.authority.AuthUtils;
import com.farm.authority.domain.Organization;
import com.farm.authority.service.OrganizationServiceInter;
import com.farm.authority.service.UserServiceInter;
import com.farm.core.page.OperateType;
import com.farm.core.page.RequestMode;
import com.farm.core.page.ViewMode;
import com.farm.core.sql.query.DBRule;
import com.farm.core.sql.query.DataQuery;
import com.farm.core.sql.result.DataResult;
import com.farm.report.FarmReport;
import com.farm.util.web.FarmFormatUnits;
import com.farm.web.WebUtils;
import com.farm.web.easyui.EasyUiTreeNode;
import com.farm.web.easyui.EasyUiUtils;


@RequestMapping("/organization")
@Controller
public class OrganizationController extends WebUtils {
	private final static Logger log = Logger.getLogger(OrganizationController.class);

	@Resource
	private OrganizationServiceInter organizationServiceImpl;
	@Resource
	private UserServiceInter userServiceImpl;

	public OrganizationServiceInter getOrganizationServiceImpl() {
		return organizationServiceImpl;
	}

	public void setOrganizationServiceImpl(OrganizationServiceInter organizationServiceImpl) {
		this.organizationServiceImpl = organizationServiceImpl;
	}

	
	@RequestMapping("/query")
	@ResponseBody
	public Map<String, Object> queryall(@ModelAttribute("query") DataQuery query, HttpServletRequest request) {
		try {
			query = EasyUiUtils.formatGridQuery(request, query);
			if (query.getQueryRule().size() == 0) {
				query.addRule(new DBRule("PARENTID", "NONE", "="));
			}
			DataResult result = organizationServiceImpl.createOrganizationSimpleQuery(query).search();
			result.runDictionary("1:可用,0:禁用", "STATE");
			result.runDictionary("1:标准", "TYPE");
			return ViewMode.getInstance().putAttrs(EasyUiUtils.formatGridData(result)).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	
	@RequestMapping("/exportEvent")
	public void download(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		try {
			DataQuery query = DataQuery.getInstance();
			query.addRule(new DBRule("STATE", "1", "="));
			query.setPagesize(5000);
			DataResult result = organizationServiceImpl.createOrganizationSimpleQuery(query).search();
			result.runDictionary("1:可用,0:禁用", "STATE");
			result.runDictionary("1:标准", "TYPE");
			List<Map<String, Object>> list = result.getResultList();
			Map<String, Integer> sortdic = new HashMap<String, Integer>();
			for (Map<String, Object> org : list) {
				sortdic.put((String) org.get("ID"), (int) org.get("SORT"));
			}
			List<Map<String, Object>> backList = new ArrayList<Map<String, Object>>();
			{
				
				for (Map<String, Object> org : list) {
					String parentid = (String) org.get("PARENTID");
					if (parentid.trim().toUpperCase().equals("NONE") || sortdic.get(parentid) != null) {
						String treeindex = (String) org.get("TREECODE");
						List<String> indexs = FarmFormatUnits.SplitStringByLen(treeindex, 32);
						String sortkey = "";
						String levelFlag = "";
						for (String index : indexs) {
							String sort = "0000" + sortdic.get(index);
							levelFlag = levelFlag + "+  ";
							sortkey = sortkey + (sort.substring(sort.length() - 3));
						}
						sortkey = sortkey + "001001001001001001001001";
						org.put("SORTKEY", sortkey.substring(0, 3 * 8));
						org.put("TITLE", (levelFlag.length() > 3 ? levelFlag.substring(3) : "") + org.get("NAME"));
						org.put("LEVEL", indexs.size());
						backList.add(org);
					}
				}
			}

			Collections.sort(backList, new Comparator<Map<String, Object>>() {
				@Override
				public int compare(Map<String, Object> o1, Map<String, Object> o2) {
					return o1.get("SORTKEY").toString().compareTo(o2.get("SORTKEY").toString());
				}
			});

			FarmReport.newInstance("orgs.xls").addParameter("result", backList).generateForHttp(response,
					"organizations");
		} catch (Exception e) {
			log.error(e);
		}
	}

	

	@RequestMapping("/list")
	public ModelAndView index(HttpSession session) {
		return ViewMode.getInstance().returnModelAndView("authority/OrganizationResult");
	}

	
	@RequestMapping("/chooseOrg")
	public ModelAndView chooseOrg(HttpSession session) {
		return ViewMode.getInstance().returnModelAndView("authority/ChooseOrgResult");
	}

	
	@RequestMapping("/organizationTabs")
	public ModelAndView orgTabs(RequestMode pageset, String ids, String parentID) {

		if (pageset.getOperateType() == 1) {
			return ViewMode.getInstance().putAttr("pageset", pageset).putAttr("parentId", parentID)
					.returnModelAndView("authority/OrganizationTabs");
		} else {
			Organization entity = organizationServiceImpl.getOrganizationEntity(ids);
			return ViewMode.getInstance().putAttr("pageset", pageset).putAttr("entity", entity)
					.putAttr("parentId", entity.getParentid()).putAttr("ids", ids)
					.returnModelAndView("authority/OrganizationTabs");
		}
	}

	
	@RequestMapping("/postConsoleTabs")
	public ModelAndView postConsoleTabs(RequestMode pageset, String ids) {
		return ViewMode.getInstance().putAttr("pageset", pageset).returnModelAndView("authority/PostResult");
	}

	
	@RequestMapping("/postActionsTabs")
	public ModelAndView postActionsTabs(RequestMode pageset, String ids) {
		return ViewMode.getInstance().putAttr("pageset", pageset).putAttr("ids", ids)
				.returnModelAndView("authority/OrgUserResult");
	}

	
	@RequestMapping("/OrgTreeNodeSubmit")
	@ResponseBody
	public Object moveTreeNodeSubmit(String ids, String id, HttpSession session) {
		try {
			organizationServiceImpl.moveOrgTreeNode(ids, id, getCurrentUser(session));
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	
	@RequestMapping("/edit")
	@ResponseBody
	public Map<String, Object> editSubmit(Organization org, HttpSession session) {
		try {
			Organization entity = organizationServiceImpl.editOrganizationEntity(org, getCurrentUser(session));
			return ViewMode.getInstance().setOperate(OperateType.ADD).putAttr("entity", entity).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.ADD).setError(e.getMessage(), e).returnObjMode();
		}
	}

	
	@RequestMapping("/add")
	@ResponseBody
	public Map<String, Object> addSubmit(Organization org, HttpSession session) {
		try {
			Organization entity = organizationServiceImpl.insertOrganizationEntity(org, getCurrentUser(session));
			return ViewMode.getInstance().setOperate(OperateType.ADD).putAttr("entity", entity).returnObjMode();
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setOperate(OperateType.ADD).setError(e.getMessage(), e).returnObjMode();
		}
	}

	
	@RequestMapping("/del")
	@ResponseBody
	public ModelAndView delSubmit(String ids, HttpSession session) {
		try {
			for (String id : parseIds(ids)) {
				organizationServiceImpl.deleteOrganizationEntity(id, getCurrentUser(session));
			}
			return ViewMode.getInstance().returnModelAndView("authority/OrganizationResult");

		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e)
					.returnModelAndView("authority/OrganizationResult");
		}
	}

	
	@RequestMapping("/removeOrgUser")
	@ResponseBody
	public Object removeOrgUser(String id, String ids, HttpSession session) {
		try {
			organizationServiceImpl.removeOrgUsers(id, ids, getCurrentUser(session));
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	
	@RequestMapping("/orgTreeNodeTreeView")
	public ModelAndView forSend(String ids) {
		return ViewMode.getInstance().putAttr("ids", ids)
				.returnModelAndView("authority/OrganizationTreenodeChooseTreeWin");
	}

	
	@RequestMapping("/userORGTreeView")
	public ModelAndView userOrgTree(String ids) {
		return ViewMode.getInstance().putAttr("ids", ids).returnModelAndView("authority/UserorgChooseTreeWin");
	}

	
	@RequestMapping("/userOrg")
	@ResponseBody
	public Object userORGSubmit(RequestMode pageset, String ids, String id, HttpSession session) {

		try {
			for (String userId : parseIds(ids)) {
				organizationServiceImpl.addUserPost(userId, id, getCurrentUser(session));
			}
			return ViewMode.getInstance().returnObjMode();

		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	
	@RequestMapping("/addOrgUser")
	@ResponseBody
	public Object addOrgUser(String userids, String id, HttpSession session) {
		try {
			for (String userId : parseIds(userids)) {
				userServiceImpl.setUserOrganization(userId, id, getCurrentUser(session));
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	
	@RequestMapping("/setOrgUserPost")
	@ResponseBody
	public Object setOrgUserPost(String userids, String postids, HttpSession session) {
		try {
			for (String userId : parseIds(userids)) {
				userServiceImpl.setUserPost(userId, postids, getCurrentUser(session));
			}
			return ViewMode.getInstance().returnObjMode();
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	
	@RequestMapping("/postTree")
	@ResponseBody
	public Object userORGLoadTree(String id) {
		return organizationServiceImpl.loadPostTree(id);
	}

	
	@RequestMapping("/form")
	public ModelAndView view(RequestMode pageset, String ids, String parentId) {
		try {
			Organization parent = null;

			if (!parentId.equals("") && parentId != null) {
				parent = organizationServiceImpl.getOrganizationEntity(parentId);
			}

			switch (pageset.getOperateType()) {
			case (1): {
				return ViewMode.getInstance().putAttr("pageset", pageset).putAttr("parent", parent)
						.returnModelAndView("authority/OrganizationForm");
			}
			case (0): {
				Organization entity = organizationServiceImpl.getOrganizationEntity(ids);
				return ViewMode.getInstance().putAttr("pageset", pageset).putAttr("entity", entity)
						.putAttr("parent", parent).returnModelAndView("authority/OrganizationForm");
			}
			case (2): {

				Organization entity = organizationServiceImpl.getOrganizationEntity(ids);
				return ViewMode.getInstance().putAttr("pageset", pageset).putAttr("entity", entity)
						.putAttr("parent", parent).returnModelAndView("authority/OrganizationForm");

			}
			}
		} catch (Exception e) {
			return ViewMode.getInstance().setError(e.getMessage(), e).returnModelAndView("authority/OrganizationForm");
		}
		return ViewMode.getInstance().returnModelAndView("authority/OrganizationForm");
	}

	
	@RequestMapping("/organizationTree")
	@ResponseBody
	public Object loadTreeNode(String id) {
		try {
			if (id == null) {
				id = "NONE";
				List<EasyUiTreeNode> list = new ArrayList<>();
				EasyUiTreeNode nodes = new EasyUiTreeNode("NONE", "组织机构", "open", "icon-customers");
				nodes.setChildren(EasyUiTreeNode.formatAsyncAjaxTree(
						EasyUiTreeNode.queryTreeNodeOne(id, "SORT", "alone_auth_organization", "ID", "PARENTID", "NAME",
								"CTIME").getResultList(),
						EasyUiTreeNode.queryTreeNodeTow(id, "SORT", "alone_auth_organization", "ID", "PARENTID", "NAME",
								"CTIME").getResultList(),
						"PARENTID", "ID", "NAME", "CTIME"));
				list.add(nodes);
				return list;
			}
			return EasyUiTreeNode.formatAsyncAjaxTree(
					EasyUiTreeNode
							.queryTreeNodeOne(id, "SORT", "alone_auth_organization", "ID", "PARENTID", "NAME", "CTIME")
							.getResultList(),
					EasyUiTreeNode
							.queryTreeNodeTow(id, "SORT", "alone_auth_organization", "ID", "PARENTID", "NAME", "CTIME")
							.getResultList(),
					"PARENTID", "ID", "NAME", "CTIME");
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	
	@RequestMapping("/organizationTreeAuth")
	@ResponseBody
	public Object loadTreeNode(String id, HttpSession session) {
		boolean authFlag = false;
		Organization org = AuthUtils.getCurrentOrganization(session);
		if (id == null) {
			id = "NONE";
			if (org != null) {
				authFlag = true;
				id = org.getParentid();
			}
		}

		DataQuery query1 = EasyUiTreeNode.queryTreeNodeOneAuth(id, "SORT", "alone_auth_organization", "ID", "PARENTID",
				"NAME", "CTIME", null, null, authFlag);
		DataQuery query2 = EasyUiTreeNode.queryTreeNodeTowAuth(id, "SORT", "alone_auth_organization", "ID", "PARENTID",
				"NAME", "CTIME", null, authFlag);

		if (org != null) {
			query1.addRule(new DBRule("treecode", AuthUtils.getCurrentOrganization(session).getTreecode(), "like-"));
			query2.addRule(new DBRule("b.treecode", AuthUtils.getCurrentOrganization(session).getTreecode(), "like-"));
		}
		try {
			return EasyUiTreeNode.formatAsyncAjaxTree(query1.search().getResultList(), query2.search().getResultList(),
					"PARENTID", "ID", "NAME", "CTIME");
		} catch (Exception e) {
			log.error(e.getMessage());
			return ViewMode.getInstance().setError(e.getMessage(), e).returnObjMode();
		}
	}

	
	@RequestMapping("/loadTree")
	@ResponseBody
	public List<Organization> loadTree() {
		return organizationServiceImpl.getTree();
	}
	// ----------------------------------------------------------------------------------

	
	@RequestMapping("/loadPost")
	@ResponseBody
	public List<Map<String, Object>> loadPost(String orgId) {
		return organizationServiceImpl.getPostList(orgId);
	}

	
	@RequestMapping("/loadPostWithPOrgPost")
	@ResponseBody
	public List<Map<String, Object>> loadPostWithPOrgPost(String orgId) {
		return organizationServiceImpl.getPostListWithPOrgPost(orgId);
	}

}
